\contentsline {section}{\numberline {1}Introduction}{12}
\contentsline {subsection}{\numberline {1.1}The Project}{12}
\contentsline {section}{\numberline {2}Literature Review}{14}
\contentsline {subsection}{\numberline {2.1}Robotic Perception}{14}
\contentsline {subsubsection}{\numberline {2.1.1}Perception in Autonomous Vehicles}{15}
\contentsline {subsection}{\numberline {2.2}Computer Vision}{16}
\contentsline {subsubsection}{\numberline {2.2.1}Histogram of Oriented Gradients}{16}
\contentsline {subsection}{\numberline {2.3}Spatial Measurement Data}{18}
\contentsline {subsubsection}{\numberline {2.3.1}Occupancy Grids}{18}
\contentsline {subsubsection}{\numberline {2.3.2}Point Clouds}{19}
\contentsline {section}{\numberline {3}Method}{20}
\contentsline {subsection}{\numberline {3.1}Data Set Overview}{20}
\contentsline {subsubsection}{\numberline {3.1.1}Vision Data}{22}
\contentsline {subsubsection}{\numberline {3.1.2}Laser Data}{23}
\contentsline {subsection}{\numberline {3.2}Playback Interface}{25}
\contentsline {subsubsection}{\numberline {3.2.1}Robot Operating System}{25}
\contentsline {subsubsection}{\numberline {3.2.2}MATLAB}{28}
\contentsline {subsection}{\numberline {3.3}Algorithm Implementation}{30}
\contentsline {subsubsection}{\numberline {3.3.1}MATLAB Toolboxes}{30}
\contentsline {subsubsection}{\numberline {3.3.2}People Detection}{30}
\contentsline {subsubsection}{\numberline {3.3.3}Custom Vehicle Detector}{33}
\contentsline {subsubsection}{\numberline {3.3.4}Training Multiple Vehicle Detectors}{36}
\contentsline {subsubsection}{\numberline {3.3.5}Occupancy Grid}{39}
\contentsline {section}{\numberline {4}Results}{41}
\contentsline {subsection}{\numberline {4.1}Whole System Testing}{41}
\contentsline {subsection}{\numberline {4.2}Front Profile Classifier}{42}
\contentsline {subsection}{\numberline {4.3}Side Profile Classifier}{43}
\contentsline {subsection}{\numberline {4.4}Discussion}{44}
\contentsline {section}{\numberline {5}Conclusion}{45}
\contentsline {subsection}{\numberline {5.1}Outcomes}{45}
\contentsline {subsection}{\numberline {5.2}Recommendations}{46}
\contentsline {subsection}{\numberline {5.3}Future Work}{46}
\contentsline {section}{\numberline {6}References}{48}
\contentsline {section}{\numberline {7}Appendix}{50}
\contentsline {subsection}{\numberline {7.1}LCM Types}{50}
