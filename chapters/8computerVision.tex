\subsubsection{MATLAB Toolboxes}
Within MATLAB numerous functions and classes are included for image processing and computer vision. These are known as the Image Processing Toolbox and Computer Vision Toolbox, respectively. The Image Processing Toolbox is built mainly with functions for performing pixel based transformations while the Computer Vision Toolbox includes system objects for more computationally-heavy tasks such as running object detectors, extracting features and analysing video files.\\ 

\subsubsection{People Detection}
The first attempt at applying a perceptual algorithm was using the \texttt{vision.PeopleDetector} system object. This is a type of classifier that classifies upright people using HOG features and a pre-trained support vector machine. The class contains configurable properties such as \texttt{ScaleFactor} for increasing the detector's range outside of the training set's default pixel sizes. \\

Before integrating the detector into the playback environment, it was trialled on another set of images for proof of concept. A sample of the resulting detections is given in Figure~\ref{fig:peopleDetector} along with the HOG features. The detection's scores (values closer to 1.0 are considered more successful detections) also given in Figure~\ref{fig:peopleDetector} indicated the detector was not making a great performance, as by inspection the people are not particularly difficult to spot in these images which should have resulted in higher scores. \\

Despite the poor performance of the detector, some single images from the dataset containing a pedestrian were extracted and run through the trial script. This was unsuccessful, with zero true positive detections in any case. The \texttt{ScaleFactor}, \texttt{MaxSize} and \texttt{WindowStride} properties were adjusted to see if any true positive detections could be made, but this resulted in still no successful detections. It was noted that while the detector was not detecting the pedestrian, it was not making any false positive detections either. This indicated that the training set used to train the support vector machine classifier was significantly different to the profile of the pedestrian in this case - after inspecting the HOG features of each image (Figures~\ref{fig:pplDetectHOG},~\ref{fig:pedestrianHOG}) it was determined this was due to the high positioning of the camera on the F-250. \\

Because there was only one instance of a pedestrian in the dataset, it would be impractical to train a custom detector for the particular camera angle and image properties.\\

\begin{figure}[H]
  \begin{center}
    \mbox{
    \subfigure[Test image, detection score $\approx 0.14$\label{fig:pplDetect}]{\includegraphics[scale=0.4]{peopleDetector}}
    \subfigure[Test image HOG features\label{fig:pplDetectHOG}]{\includegraphics[scale=0.45]{peopleDetectorHOG}}
        }
  \caption{Output of test images}
  \label{fig:peopleDetector}
   \end{center}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pedestrianHOG}
	\caption{Pedestrian with HOG features}
	\label{fig:pedestrianHOG}
\end{figure}

\newpage

\subsubsection{Custom Vehicle Detector}
\label{sec:compVis}
% Introduce ROI, using the inbuilt trainer 
Once the pre-built people detector was trialled and evaluated as not an option for use in the system, it was evident that for this dataset any detectors would require custom-trained classifiers. Within MATLAB the \texttt{vision.CascadeObjectDetector} class has a function for creating custom classifiers - \texttt{trainCascadeObjectDetector}. \\

This function takes a training set of positive and negative images, with the regions of interest in the positive samples defined by rectangular bounds represented by $2 \times 2$ matrices of pixel coordinates. It can then be configured and tuned by adjusting the \textit{per stage flase alarm rate, per stage true positive rate, setting the number of cascade stages and setting the negative sample factor per stage}. Once training is complete an XML file is generated that stores the parameters of the classifier stages. This file can then be used by a \texttt{CascadeObjectDetector} system object to classify objects using the Viola-Jones algorithm. \\

To simplify the problem of detection, it was decided that only the most critical region of vision would be analysed. Cam0 images are directly forward-facing with a similar field of view to that of a driver in a traditional road vehicle, albeit with an elevated perspective.  Because of the individual images' 'pinched' format and tall pixel height (as discussed in Section~\ref{sec:dataSet}) it was decided to exclusively use cam0 images, modified through the use of a crop function. This had the added bonus of speeding up the environment buffer load times. The crop region was defined qualitatively, approximately in the centre of the original image: \\
\[
\begin{array}{lc}
 \begin{bmatrix} x_{min} & y_{min} & width & height \end{bmatrix}
 =
 \begin{bmatrix} 105 & 623 & 405 & 442 \end{bmatrix}
\end{array}
\]

\begin{figure}[H]
  \begin{center}
    \mbox{
    \subfigure[Pre-Crop\label{uncrop}]{\includegraphics[scale=0.15]{cropP542}}\quad
    \subfigure[Post-Crop\label{crop}]{\includegraphics[scale=0.5]{p542}}\quad
       }
  \caption{Pre and Post-Crop (Image~\ref{uncrop} scaled down)}
  \label{fig:cropping}
   \end{center}
\end{figure}

After manually reviewing the images for appropriate positive and negative samples, roughly 30 of each were selected. The image indexes were input into the script, \texttt{CropImageSet} which automatically loaded, cropped, renamed and saved the chosen files to a local directory in the development folder. Once this was done, the vehicles in the positive training images could be defined with bounding boxes representing the region(s) of interest using the Cascade Trainer graphical user interface. Finally, the directory of the negative images was specified and the HOG feature detection training could be undertaken. \\

The following parameters were set for generating the first custom classifier:\\

$Per Stage False Alarm Rate   = 0.29605$\\
$Per Stage True Positive Rate = 1.0$\\
$Number of Cascade Stages     = 10$\\
$Negative Sample Factor       = 2$\\ 

Because there were so few negative samples, the detector could only be resolved with a high $PerStageFalseAlarmRate$. When run in the environment the classifier proved extremely noisy, with a very high rate of false positive detections - See Figure~\ref{fig:firstClassWrong}. This was attributed to the high $PerStageFalseAlarmRate$. This first attempt was a sign of progression however its performance was not acceptable. A new strategy had to be devised.\\

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{firstClassWrong}
	\caption{False Positive Detections}
	\label{fig:firstClassWrong}
\end{figure}


\subsubsection{Training Multiple Vehicle Detectors}

\begin{figure}[H]
  \begin{center}
    \mbox{
    \subfigure[Front Profile\label{fig:FP}]{\includegraphics[scale=0.5]{frontView}}
    \subfigure[Side Profile\label{fig:SP}]{\includegraphics[scale=0.5]{sideView}}
}      
      \caption{Examples of profiles with ROIs defined}
      
  \label{fig:profiles}
   \end{center}
\end{figure}

Despite the poor performance of the first vehicle detector, it was hypothesised the results could be improved (reducing the number of false/missed detections)by concentrating more carefully on the training set vehicles' body shape and orientation, then using a dedicated classifier for each profile. Three general cases of a sedan profile were devised - oncoming vehicles, followed vehicles and vehicles viewed from the side, defined as front profile, rear profile and side profile. After considering the work from \citet{hog2} in Section~\ref{sec:litRev}, it was obvious that a rear profile was unnecessary - it would have almost an identical HOG feature set as the front profile, so the number of classifiers was limited to 2. Examples of positive training set samples for each profile can be seen in Figure~\ref{fig:profiles}. Along with this renewed approach, a significantly larger training set would be collected. This would include a higher ratio of negative to positive images, which would allow for a greater number of  stages to be used in training the classifier, resulting in higher accuracy.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{trajectory1}
\caption{The trajectory taken by the F-250}
\label{fig:traject}
\end{figure}

Improvements were made to the training set documentation and procedure by using spreadsheets to record the image indexes of the positive and negative samples whilst also attempting to overcome the obvious issue that faced the need for more training data - it would be hard to get enough samples without using the entire set just for training. The F-250's driving loop had to be split up into regions of training and trialling (See Figure~\ref{fig:traject}), so that the detector would not be tested on vehicles it had been trained with. The selected training image indexes were input to the \texttt{CropImageSet} script, which output training files to their corresponding directory folders. \\


Table~\ref{tab:trainingParam} summarises the training set size and tuning parameters for each classifier. 
\begin{table}[h]
\begin{center}
%table content
\begin{tabular}{|p{5.5cm}|p{2cm}|p{2.5cm}|}
\hline
&        \textbf{Front Profile} & \textbf{Side Profile} \\ \hline
Positive Samples              &  50 &    65 \\  \hline
Negative Samples              &  458 &   158 \\  \hline
$Per Stage False Alarm Rate   $ &  0.4588  &     0.33553  \\  \hline
$Per Stage True Positive Rate $ &   0.993 & 0.995      \\  \hline
$Number Of Cascade Stages     $ & 13 &     9 \\  \hline
$Negative Samples Factor      $ &   2 &  2\\ \hline
\end{tabular}
\caption{Training set and parameter details}
\label{tab:trainingParam}
\end{center}
\end{table}  
\newpage

Table~\ref{tab:trainingParam} gives the resulting parameters for each trained classifier. The vehicle detectors were then implemented into the rest of the software for testing. Figure~\ref{fig:updatedTree} shows the dependency of the processing stage on each classifier object, which are in turn dependent on the corresponding XML files and the \texttt{vision.CascadeObjectDetector} class. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{UpdateddependencyTree}
\caption{Updated MATLAB software dependency tree}
\label{fig:updatedTree}
\end{figure}
